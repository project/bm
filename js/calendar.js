(function ($) {
  $(document).ready(function () {
    $('#edit-start-date').datepicker({
      autoclose: true,
      toggleActive: true
    });

    $('#edit-end-date').datepicker({
      autoclose: true,
      toggleActive: true
    });

    $('#edit-bm-child').attr('type', 'number');
    $('#edit-bm-adult').attr('type', 'number');

    $('.calendar-view-pager__reset a').attr('href', '?calendar_timestamp=today');
  });
})(jQuery);
