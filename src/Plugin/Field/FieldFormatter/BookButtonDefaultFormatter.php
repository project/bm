<?php

declare(strict_types=1);

namespace Drupal\bm\Plugin\Field\FieldFormatter;

use Drupal\bm\Plugin\Field\FieldType\BookButtonItem;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'bm_book_button_default' formatter.
 *
 * @FieldFormatter(
 *   id = "bm_book_button_default",
 *   label = @Translation("Default"),
 *   field_types = {"bm_book_button"},
 * )
 */
final class BookButtonDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['label' => 'book', 'class' => 'btn btn-primary' ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->getSetting('label'),
    ];

    $element['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#default_value' => $this->getSetting('class'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Label: @label,<br> Class: @class', [
        '@label' => $this->getSetting('label'),
        '@class' => $this->getSetting('class')
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    return $element['booking_btn'] = [
      '#title' => $this->getSetting('label'),
      '#type' => 'link',
      '#url' => \Drupal\Core\Url::fromRoute('bm.booking_buttom.form', [
        'bid' => $items->getEntity()->id()
      ]),
      '#options' => [
        'attributes' => [
          'class' => $this->getSetting('class'),
        ]
      ]
    ];
  }

}
