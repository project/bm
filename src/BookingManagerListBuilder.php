<?php declare(strict_types = 1);

namespace Drupal\bm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the booking manager entity type.
 */
final class BookingManagerListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['type'] = $this->t('Booking Type');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Updated');
    $header['uid'] = $this->t('Author');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\bm\BookingManagerInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    $row['type'] = $entity->bundle();
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    return $row + parent::buildRow($entity);
  }

}
