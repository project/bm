<?php declare(strict_types = 1);

namespace Drupal\bm;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of booking manager type entities.
 *
 * @see \Drupal\bm\Entity\BookingManagerType
 */
final class BookingManagerTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No booking manager types available. <a href=":link">Add Booking manager type</a>.',
      [':link' => Url::fromRoute('entity.bm_type.add_form')->toString()],
    );

    return $build;
  }

}
