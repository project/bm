<?php declare(strict_types = 1);

namespace Drupal\bm;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a booking manager entity type.
 */
interface BookingManagerInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
