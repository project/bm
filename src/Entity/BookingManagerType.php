<?php declare(strict_types = 1);

namespace Drupal\bm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Booking Manager type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "bm_type",
 *   label = @Translation("Booking Manager type"),
 *   label_collection = @Translation("Booking Manager types"),
 *   label_singular = @Translation("booking manager type"),
 *   label_plural = @Translation("booking managers types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count booking managers type",
 *     plural = "@count booking managers types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\bm\Form\BookingManagerTypeForm",
 *       "edit" = "Drupal\bm\Form\BookingManagerTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\bm\BookingManagerTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer bm types",
 *   bundle_of = "bm",
 *   config_prefix = "bm_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/bm_types/add",
 *     "edit-form" = "/admin/structure/bm_types/manage/{bm_type}",
 *     "delete-form" = "/admin/structure/bm_types/manage/{bm_type}/delete",
 *     "collection" = "/admin/structure/bm_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class BookingManagerType extends ConfigEntityBundleBase {

  /**
   * The machine name of this booking manager type.
   */
  protected string $id;

  /**
   * The human-readable name of the booking manager type.
   */
  protected string $label;

}
